function showSuccessAlert(text){
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true
    });
    Toast.fire({
        icon: 'success',
        title: text
    });
}


function deleteProductFromBasket(product_id, color_id, size_id) {
    swal({
        title: $('.to-trash').attr('data-title-msg'),
        text: $('.to-trash').attr('data-text-msg'),
        icon: "warning",
        buttons: [$('.to-trash').attr('data-cancel-msg'), "OK"],
        dangerMode: true,
    })
    .then((willDelete) => {
        if (willDelete) {
            window.location.href = "/card/delete-product-from-basket?product_id="+product_id+"&color_id="+color_id+"&size_id="+size_id;
        }
    });
}



function decreaseProduct(product_id, color_id, size_id) {

    var delivery_id = $('#addressA option:selected').val();

    if(color_id == 0){
        color_id = '';
    }
    if(size_id == 0){
        size_id = '';
    }

    $.ajax({
        url: '/card/down-clicked',
        type: 'GET',
        dataType: 'json',
        data: {product_id:product_id,color_id:color_id,size_id:size_id,delivery_id:delivery_id},
        success: function(data){
            $('.countAllProduct').html(data.countAllProduct);
            $('#countProduct'+product_id+''+color_id+''+size_id).html(data.countProduct);
            $('.sumProduct').html(data.sumProduct);
            $('.deliveryPrice').html(data.deliveryPrice);
            $('.percent').html(data.percent);
            $('.sum').html(data.sum);
        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');

        }
    });
}



function increaseProduct(product_id, color_id, size_id) {

    var delivery_id = $('#addressA option:selected').val();

    if(color_id == 0){
        color_id = '';
    }
    if(size_id == 0){
        size_id = '';
    }

    $.ajax({
        url: '/card/up-clicked',
        type: 'GET',
        dataType: 'json',
        data: {product_id:product_id,color_id:color_id,size_id:size_id,delivery_id:delivery_id},
        success: function(data){
            if(data.status == 1){
                $('.countAllProduct').html(data.countAllProduct);
                $('#countProduct'+product_id+''+color_id+''+size_id).html(data.countProduct);
                $('.sumProduct').html(data.sumProduct);
                $('.deliveryPrice').html(data.deliveryPrice);
                $('.percent').html(data.percent);
                $('.sum').html(data.sum);

            }else{
                Swal.fire({
                    icon: 'error',
                    text:  data.error,
                });
            }

        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
}



function updateDeliveryPrice(delivery_id) {

    $.ajax({
        url: '/card/update-delivery-price',
        type: 'GET',
        dataType: 'json',
        data: {delivery_id:delivery_id},
        success: function(data){

            $('.deliveryPrice').html(data.deliveryPrice);
            $('.percent').html(data.percent);
            $('.sum').html(data.sum);

        },
        error: function () {
            swal('Упс!', 'Что-то пошло не так.', 'error');
        }
    });
}



function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


function checkout() {

    var fio = $('#fio').val();
    var phone = $('#telephone').val();
    var email = $('#email').val();

    var addressA = $('#addressA option:selected').val();
    var addressB = $('#addressB').val();
    var comment = $('#other_information').val();
    var delivery_id = $('#addressA option:selected').val();
    var paymentMethod = 0;

    phoneStatus = true;
    for (var j = 0; j < phone.length; j++) {
        if (phone.charAt(j) == '_') {
            phoneStatus = false;
        }
    }

    if(!fio){
        Swal.fire({
            icon: 'error',
            text: $('#fio').attr('data-fio-msg'),
        });
    }else if (!phone || !phoneStatus) {
        Swal.fire({
            icon: 'error',
            text: $('#telephone').attr('data-telephone-msg'),
        });
    }else if(email && !isEmail(email)){
        Swal.fire({
            icon: 'error',
            text: $('#email').attr('data-email-msg'),
        });
    }else if(!addressA){
        Swal.fire({
            icon: 'error',
            text:  $('#addressA').attr('data-locality-msg'),
        });
    }else if(!addressB){
        Swal.fire({
            icon: 'error',
            text:  $('#addressB').attr('data-address-msg'),
        });
    }else {
        // CHECK PAYMENT METHOD
        var isPicked = false;
        if($('#cash_to_courier').attr('data-status') == 1) {
            isPicked = true;
            paymentMethod = 1;
        }else if($('#banking_card').attr('data-status') == 1){
            isPicked = true;
            paymentMethod = 2;
        }
        // END CHECK PAYMENT METHOD

        if(!isPicked){
            Swal.fire({
                icon: 'error',
                text: $('.checked-payment').attr('data-method-msg'),
            });
        }else{
            if(paymentMethod == 0){
                Swal.fire({
                    icon: 'error',
                    text: $('.checked-payment').attr('data-method-msg'),
                });
            }else if(paymentMethod == 2){
                swal('', 'К сожелению, оплата банковской картой не доступно :(', 'error');
            }else{
                Swal.showLoading();
                $.ajax({
                    url: '/card/order-by-delivery',
                    type: 'GET',
                    data: {fio:fio,phone:phone,email:email,addressA:addressA,addressB:addressB,comment:comment,delivery_id:delivery_id},
                    success: function(data){
                        if(data==0){
                            Swal.close();
                            swal('Упс!','Что-то пошло не так!', 'error');
                        }else{
                            Swal.close();
                            window.location.href = '/card/finished?access_token='+data;
                        }
                    },
                    error: function () {
                        Swal.close();
                        swal('Упс!', 'Что-то пошло не так.', 'error');
                    }
                });
            }
        }
    }
}


$('body').on('click','#cash_to_courier', function () {
    if($(this).attr('data-status') == 0){
        $(this).attr('data-status',1);
        $(this).html($(this).attr('title')+' <img src="/public/img/Vector.png">');
        $('#banking_card').attr('data-status',0);
        $('#banking_card').html($('#banking_card').attr('title'));
    }
});


// $('body').on('click','#banking_card', function () {
//     if($(this).attr('data-status') == 0){
//         $(this).attr('data-status',1);
//         $(this).html($(this).attr('title')+' <img src="/public/img/Vector.png">');
//         $('#cash_to_courier').attr('data-status',0);
//         $('#cash_to_courier').html($('#cash_to_courier').attr('title'));
//     }
// });




