<?php
	require_once('header2.php');
?>
<div class="result">
    <div class="container products-show">
        <div class="cont_tov">
            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/tov1.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <div class="tov_new">Новинка!</div>
                    <p>Платье - сарафон, розовое</p>
                    <div class="price">5 630 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/tov2.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <p>Футболка, красная</p>
                    <div class="price">7 000 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/tov3.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <div class="tov_new tov_pov">Популярное!</div>
                    <p>Укороченные джинсовые шорты</p>
                    <div class="price">1060 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/tov4.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <p>Свадебное платье</p>
                    <div class="price">21 060 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/tov5.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <p>Блузка голубая</p>
                    <div class="price">3 000 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/tov6.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <p>Туника в клетку, фиолетовая</p>
                    <div class="old_price">9 000 <span>тг</span></div>
                    <div class="price">4 990 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/tov7.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <p>Длинный трикотажный кардиган, серый</p>
                    <div class="price">8 000 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/tov8.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <div class="tov_new tov_exs">Эксклюзив!</div>
                    <p>Блузка оранжевая</p>
                    <div class="price">3 120 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/img1.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <div class="tov_new">Новинка!</div>
                    <p>Женское пальто Klimini, бежевое</p>
                    <div class="price">13 000 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/img2.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <p>Платье Versace, фиолетовое</p>
                    <div class="price">9 340 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/img3.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <p>Платье оранжевое</p>
                    <div class="old_price">56 000 <span>тг</span></div>
                    <div class="price">13 000 <span>тг</span></div>
                </div>
            </div>

            <div class="tov_item">
                <a href="" class="tov_item_img">
                    <img src="./img/img4.png" alt="">
                </a>
                <a href="" class="tov-button">Подробнее</a>
                <div class="tov_item_info">
                    <div class="tov_new">Новинка!</div>
                    <p>Темно-синий женский жилет</p>
                    <div class="price">7 390 <span>тг</span></div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
	require_once('footer.php');
?>