<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="description" content="Интернет-магазин брендовой одежды">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">		
	<title>Bopai | Интернет-магазин брендовой одежды</title>
	<link rel="shortcut icon" href="./img/favicon.ico" type="image/x-icon">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="hc-offcanvas-nav-master/dist/hc-offcanvas-nav.css">
	<link rel="stylesheet" href="./libs/owlcarousel/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="./libs/owlcarousel/assets/owl.theme.default.min.css">
	<link rel="stylesheet" href="https://cdn.bootcss.com/Glide.js/3.3.0/css/glide.core.css">
	<link rel="stylesheet" href="https://cdn.bootcss.com/Glide.js/3.3.0/css/glide.theme.css">
	<link rel="stylesheet" type="text/css" href="slick/slick.css">
	<link rel="stylesheet" type="text/css" href="slick/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script>
		
	</script>
</head>
<body>
	<div class="header" id="header">
		<div class="top-menu">
			<div class="container">
				<div class="row">
					<div class="col-sm-6 col-md-2 w-50">
						<select class="country js-states">
			    			<option value="40">Казахстан</option>
							<option value="42">Россия</option>
		  				    <option value="44">Украина</option>
						</select>
					</div>
					<div class="col-sm-12 col-md-7 top-navv">
						<nav>
							<ul class="top-nav">
								<li>
									<a href="/about.php">о компании</a>
								</li>
								<li>
									<a href="#">доставка и оплата</a>
								</li>
								<li>
									<a href="#">частые вопросы</a>
								</li>
								<li>
									<a href="#">адреса магазинов</a>
								</li>
							</ul>
						</nav>
					</div>
					<div class="col-sm-6 col-md-3 text-right signInOrRegister w-50">
						<p><a href="#" data-toggle="modal" data-target="#login">войти</a> <span>/</span> <a href="#" data-toggle="modal" data-target="#registration">регистрация</a></p>
						<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="exampleModalLabel">Вход</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<form>
											<label for="uname" class="form-control-label">Email:</label>
											<input type="email" class="form-control" id="uname">
											<label for="upwd" class="form-control-label">Пароль:</label>
											<input type="password" class="form-control" id="upwd">
										</form>
									</div>
									<div class="modal-footer">
									    <button type="button" class="btn btn-primary">Войти</button>
									</div>
								</div>
							</div>
						</div>
						<div class="modal fade" id="registration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title" id="exampleModalLabel">Регистрация</h4>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<form>
											<label for="umal" class="form-control-label">Ваш email:</label>
											<input type="email" class="form-control" id="umail">
											<label for="upwd" class="form-control-label">Придумайте пароль:</label>
											<input type="password" class="form-control" id="upwd">
										</form>
									</div>
									<div class="modal-footer">
									    <button type="button" class="btn btn-primary">Зарегистрироваться</button>
									</div>
								</div>
							</div>
						</div>

					</div>
					
				</div>
			</div>
		</div>
		<div class="bottom-menu pt-3 pb-4">
			<div class="container">
				<div class="row">
					<div class="col-sm-12 col-md-2 stStatic">
						<a href="#">
							<img src="img/logo.png" alt="Логотип">
						</a>
						<nav id="catalog-nav">
							<ul class="first-nav">
								<li class="clothes">
									<span>Одежда</span>
									<ul>
										<li class="man">
											<a href="#">Мужская</a>
											<ul>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
											</ul>
										</li>
										<li class="woman">
											<a href="#">Женская</a>
											<ul>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="footwear">
									<span>Обувь</span>
									<ul>
										<li class="man">
											<a href="#">Мужская</a>
											<ul>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
											</ul>
										</li>
										<li class="woman">
											<a href="#">Женская</a>
											<ul>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li class="linen">
									<span>Белье</span>
									<ul>
										<li class="man">
											<a href="#">Мужское</a>
											<ul>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
											</ul>
										</li>
										<li class="woman">
											<a href="#">Женское</a>
											<ul>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
												<li><a href="#">Контент</a></li>
											</ul>
										</li>
									</ul>
								</li>
								<li><a href="#">Большие размеры</a></li>
								<li><a href="#">Спецодежда</a></li>
								<li><a href="#">Одежда для офиса</a></li>
								<li><a href="#">Одежда для дома</a></li>
								<li><a href="#">Свадебные наряды</a></li>
								<li><a href="#">Будущие мамы</a></li>
								<li><a href="#">Все для пляжа</a></li>
								<li class="inner-nav">
									<span>Навигация</span>
									<ul>
										<li>
											<a href="#">о компании</a>
										</li>
										<li>
											<a href="#">доставка и оплата</a>
										</li>
										<li>
											<a href="#">частые вопросы</a>
										</li>
										<li>
											<a href="#">адреса магазинов</a>
										</li>
									</ul>
								</li>

							</ul>
						</nav>
						<a class="toggle">
						    <span></span>
						    
						</a>
					</div>
					<div class="col-sm-12 col-md-6">
						<form action="">
							<input class="search p-3" type="search" placeholder="Поиск на сайте">
						
							<input class="search-btn" type="submit" value="">
						</form>
					</div>
					<div class="col-sm-6 col-md-2 d-flex align-items-center pr-0 w-50">
						<a class="favorite-link" href="#">
							<svg width="22" height="20" viewBox="0 0 22 20" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path fill="red" d="M10.8882 1.86775L11.0656 2.00475L11.2472 1.87353C12.4544 1.00163 13.9579 0.527488 15.462 0.527488C17.0612 0.527488 18.6246 1.17739 19.7511 2.33146C22.3501 5.05929 22.3484 9.34744 19.7534 12.0358L19.7194 12.0695L12.3149 19.1577L12.3149 19.1577L12.3109 19.1615C11.588 19.8795 10.4024 19.8795 9.67951 19.1615L9.67954 19.1615L9.67614 19.1582L2.27358 12.0341C-0.359859 9.3058 -0.356632 5.02018 2.2759 2.33317L2.27598 2.33309C3.55608 1.02549 5.2733 0.3 7.10108 0.3C8.27009 0.3 9.54292 0.828637 10.8882 1.86775ZM17.998 10.3403L18.0041 10.3345L18.0098 10.3283C19.6452 8.58246 19.6455 5.82337 17.9698 4.07729C17.2971 3.36787 16.3378 2.99858 15.3857 2.99858H15.3475V2.99844L15.3385 2.99871C14.0032 3.0389 12.7854 3.56244 11.8528 4.48856C11.359 4.97901 10.5551 4.97901 10.0613 4.48856L10.0613 4.48852L10.0575 4.48488C9.49219 3.94281 8.94799 3.51856 8.44388 3.22819C7.94361 2.94003 7.46096 2.77109 7.02473 2.77109C5.87474 2.77109 4.80663 3.22076 3.98817 4.03358L3.98804 4.03345L3.98079 4.04117C2.30846 5.82349 2.30515 8.58325 3.98316 10.331L3.98295 10.3312L3.99248 10.3403L10.7881 16.8237L10.9952 17.0213L11.2023 16.8237L17.998 10.3403Z" fill="#757482" stroke="white" stroke-width="0.6"/>
							</svg>
						</a>
						<p class="favorite">Избранные
							<span>Нет избранных</span>
						</p>
					</div>
					<div class="col-sm-6 col-md-2 d-flex align-items-center pr-0 w-50">
						<a class="trash-link" href="#">
							<svg width="22" height="24" viewBox="0 0 22 24" fill="none" xmlns="http://www.w3.org/2000/svg">
							<path d="M21.0571 5.85366H16.9714V5.56098C16.9714 4.08611 16.3423 2.67166 15.2224 1.62877C14.1026 0.585887 12.5837 0 11 0C9.41628 0 7.89742 0.585887 6.77756 1.62877C5.6577 2.67166 5.02857 4.08611 5.02857 5.56098V5.85366H0.942857C0.692796 5.85366 0.452977 5.94617 0.276157 6.11083C0.099337 6.2755 0 6.49884 0 6.73171V20.7805C0 21.6344 0.364233 22.4533 1.01257 23.057C1.66091 23.6608 2.54025 24 3.45714 24H18.5429C19.4597 24 20.3391 23.6608 20.9874 23.057C21.6358 22.4533 22 21.6344 22 20.7805V6.73171C22 6.49884 21.9007 6.2755 21.7238 6.11083C21.547 5.94617 21.3072 5.85366 21.0571 5.85366ZM6.91429 5.56098C6.91429 4.55186 7.34474 3.58407 8.11096 2.87052C8.87718 2.15697 9.9164 1.7561 11 1.7561C12.0836 1.7561 13.1228 2.15697 13.889 2.87052C14.6553 3.58407 15.0857 4.55186 15.0857 5.56098V5.85366H6.91429V5.56098ZM20.1143 20.7805C20.1143 21.1686 19.9487 21.5408 19.654 21.8153C19.3593 22.0897 18.9596 22.2439 18.5429 22.2439H3.45714C3.04037 22.2439 2.64068 22.0897 2.34598 21.8153C2.05128 21.5408 1.88571 21.1686 1.88571 20.7805V7.60976H5.02857V10.2439C5.02857 10.4768 5.12791 10.7001 5.30473 10.8648C5.48155 11.0294 5.72137 11.122 5.97143 11.122C6.22149 11.122 6.46131 11.0294 6.63813 10.8648C6.81495 10.7001 6.91429 10.4768 6.91429 10.2439V7.60976H15.0857V10.2439C15.0857 10.4768 15.1851 10.7001 15.3619 10.8648C15.5387 11.0294 15.7785 11.122 16.0286 11.122C16.2786 11.122 16.5185 11.0294 16.6953 10.8648C16.8721 10.7001 16.9714 10.4768 16.9714 10.2439V7.60976H20.1143V20.7805Z" fill="#DD2B1C"/>
							</svg>

						</a>
						<p class="trash">Корзина
							<span>Ваша корзина пуста</span>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>