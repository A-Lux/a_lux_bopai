<?php
	require_once('header2.php');
?>
<div class="news pt-4 pb-5">
	<div class="container ">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<p class="main-page-link"><a href="#">Главная</a> <img src="img/_.png"> Статьи и новости</p>
			</div>
		</div>
	</div>
	<div class="news__containers container">
		<div class="row">
			<div class="col-sm-12 col-md-3">
				<div class="news__container">
					<a href="#">
						<img src="img/news1.png" alt="">
					</a>
					<a class="news-button" href="#">Перейти к новости</a>
					<div>
						<h2>
							10 антитрендов 2019 года, или Вещи, от которых стоит избавиться
						</h2>
						<p>
							21 февраля 2019
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="news__container">
					<a href="#">
						<img src="img/news2.png" alt="">
					</a>
					<a class="news-button" href="#">Перейти к новости</a>
					<div>
						<h2>
							10 антитрендов 2019 года, или Вещи, от которых стоит избавиться
						</h2>
						<p>
							21 февраля 2019
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="news__container">
					<a href="#">
						<img src="img/news3.png" alt="">
					</a>
					<a class="news-button" href="#">Перейти к новости</a>
					<div>
						<h2>
							10 антитрендов 2019 года, или Вещи, от которых стоит избавиться
						</h2>
						<p>
							21 февраля 2019
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="news__container">
					<a href="#">
						<img src="img/news4.png" alt="">
					</a>
					<a class="news-button" href="#">Перейти к новости</a>
					<div>
						<h2>
							10 антитрендов 2019 года, или Вещи, от которых стоит избавиться
						</h2>
						<p>
							21 февраля 2019
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 col-md-3">
				<div class="news__container">
					<a href="#">
						<img src="img/news5.png" alt="">
					</a>
					<a class="news-button" href="#">Перейти к новости</a>
					<div>
						<h2>
							10 антитрендов 2019 года, или Вещи, от которых стоит избавиться
						</h2>
						<p>
							21 февраля 2019
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="news__container">
					<a href="#">
						<img src="img/news6.png" alt="">
					</a>
					<a class="news-button" href="#">Перейти к новости</a>
					<div>
						<h2>
							10 антитрендов 2019 года, или Вещи, от которых стоит избавиться
						</h2>
						<p>
							21 февраля 2019
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				<div class="news__container">
					<a href="#">
						<img src="img/news7.png" alt="">
					</a>
					<a class="news-button" href="#">Перейти к новости</a>
					<div>
						<h2>
							10 антитрендов 2019 года, или Вещи, от которых стоит избавиться
						</h2>
						<p>
							21 февраля 2019
						</p>
					</div>
				</div>
			</div>
			<div class="col-sm-12 col-md-3">
				
			</div>
		</div>
	</div>
</div>
<?php
	require_once('footer.php');
?>