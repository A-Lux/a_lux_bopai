<?php
	require_once('header2.php');
?>
<div class="history">
	<div class="container mt-5 mb-5">
		<div class="row">
			<div class="col-sm-12 col-md-3">
				<nav class="switch-nav">
					<ul>
						<li><a href="/personal.php">Личные данные</a></li>
						<li><a href="#">Избранное</a></li>
						<li class="switch-nav_active"><a href="#">Истории заказов</a></li>
					</ul>
				</nav>
			</div>
			<div class="col-sm-12 col-md-1"></div>
			<div class="col-sm-12 col-md-8">
				
				<table class="order-table" cols="5" cellspacing="5">
					<thead>
						<tr>
							<th class="date-header" colspan="1">Дата</th>
							<th class="count-header" colspan="1">Заказ</th>
							<th class="cost-header" colspan="1">Стоимость</th>
							<th class="status-header" colspan="1">Статус</th>
							<th class="repeat-header" colspan="1" style="opacity: 0;">Повторить заказ</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<p class="date">13 декабря 2019</p>
							</td>
							<td>
								<p class="count" data-placement="bottom" data-toggle="popover"><span>1</span> товар</button>
							</td>
							<td>
								<p class="cost">13 000 тг.</p>
							</td>
							<td>
								<p class="new">Новый</p>
							</td>
							<td>
								<button type="button" class="repeat">Повторить заказ</p>
							</td>
						</tr>
						<tr>
							<td>
								<p class="date">13 декабря 2019</p>
							</td>
							<td>
								<p class="count" data-placement="bottom" data-toggle="popover"><span>1</span> товар</p>
							</td>
							<td>
								<p class="cost">13 000 тг.</p>
							</td>
							<td>
								<p class="new">Новый</p>
							</td>
							<td>
								<button type="button" class="repeat">Повторить заказ</p>
							</td>
						</tr>
						<tr>
							<td>
								<p class="date">13 декабря 2019</p>
							</td>
							<td>
								<p class="count" data-placement="bottom" data-toggle="popover"><span>1</span> товар</p>
							</td>
							<td>
								<p class="cost">13 000 тг.</p>
							</td>
							<td>
								<p class="new">Новый</p>
							</td>
							<td>
								<button type="button" class="repeat">Повторить заказ</p>
							</td>
						</tr>
						<tr>
							<td>
								<p class="date">13 декабря 2019</p>
							</td>
							<td>
								<p class="count" data-placement="bottom" data-toggle="popover"><span>1</span> товар</button>
							</td>
							<td>
								<p class="cost">13 000 тг.</p>
							</td>
							<td>
								<p class="new">Новый</p>
							</td>
							<td>
								<button type="button" class="repeat">Повторить заказ</p>
							</td>
						</tr>
						<tr>
							<td>
								<p class="date">13 декабря 2019</p>
							</td>
							<td>
								<p class="count" data-placement="bottom" data-toggle="popover"><span>1</span> товар</button>
							</td>
							<td>
								<p class="cost">13 000 тг.</p>
							</td>
							<td>
								<p class="new">Новый</p>
							</td>
							<td>
								<button type="button" class="repeat">Повторить заказ</p>
							</td>
						</tr>
						<tr>
							<td>
								<p class="date">13 декабря 2019</p>
							</td>
							<td>
								<p class="count" data-placement="bottom" data-toggle="popover"><span>1</span> товар</button>
							</td>
							<td>
								<p class="cost">13 000 тг.</p>
							</td>
							<td>
								<p class="new">Новый</p>
							</td>
							<td>
								<button type="button" class="repeat">Повторить заказ</p>
							</td>
						</tr>
						<tr>
							<td>
								<p class="date">13 декабря 2019</p>
							</td>
							<td>
								<p class="count" data-placement="bottom" data-toggle="popover"><span>1</span> товар</button>
							</td>
							<td>
								<p class="cost">13 000 тг.</p>
							</td>
							<td>
								<p class="new">Новый</p>
							</td>
							<td>
								<button type="button" class="repeat">Повторить заказ</p>
							</td>
						</tr>
					</tbody>
				</table>
				<div class="hidden__mobile__products-card">
					<div class="mob__product-card" data-placement="bottom" data-toggle="popover">
						<div class="info">
							<div class="text">
							    <div class="qty">1 товар</div>
						    	<div class="sum">120 000 000тг</div>
							    <div class="date">2тыс до н.э</div>
							</div>
							<div class="repeat-cont">
						        <button type="button" class="repeat">Повторить заказ</p>
						    </div>
						</div>
						<div class="strip"></div>
						<div class="status">
							<div class="new">Новый</div>
						</div>
					</div>

					<div class="mob__product-card" data-placement="bottom" data-toggle="popover">
						<div class="info">
							<div class="text">
							    <div class="qty">1 товар</div>
						    	<div class="sum">120 000 000тг</div>
							    <div class="date">2тыс до н.э</div>
							</div>
							<div class="repeat-cont">
						        <button type="button" class="repeat">Повторить заказ</p>
						    </div>
						</div>
						<div class="strip"></div>
						<div class="status">
							<div class="new">Новый</div>
						</div>
					</div>

					<div class="mob__product-card" data-placement="bottom" data-toggle="popover">
						<div class="info">
							<div class="text">
							    <div class="qty">1 товар</div>
						    	<div class="sum">120 000 000тг</div>
							    <div class="date">2тыс до н.э</div>
							</div>
							<div class="repeat-cont">
						        <button type="button" class="repeat">Повторить заказ</p>
						    </div>
						</div>
						<div class="strip"></div>
						<div class="status">
							<div class="new">Новый</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
	require_once('footer.php');
?>