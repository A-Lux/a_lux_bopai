<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "delivery_price".
 *
 * @property int $id
 * @property string $city
 * @property int $price
 */
class DeliveryPrice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'delivery_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city', 'price'], 'required'],
            [['price'], 'integer'],
            [['city'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'Город',
            'price' => 'Цена',
        ];
    }

    public static function getAll(){
        return DeliveryPrice::find()->all();
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(DeliveryPrice::find()->all(),'id','city');
    }
}
