<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "about_payment".
 *
 * @property int $id
 * @property string $content
 */
class AboutPayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'content_kz'], 'required'],
            [['content','content_kz'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Содержание',
            'content_kz' => 'Содержание (KZ)',
        ];
    }

    public static function getAll(){
        return AboutPayment::find()->all();
    }


    public function getContent(){
        $content = "content".Yii::$app->session["lang"];
        return $this->$content;
    }
}
