<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_return".
 *
 * @property int $id
 * @property string $title
 * @property string $subtitle
 * @property string $content
 * @property string $condition
 */
class ProductReturn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_return';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'subtitle', 'content', 'condition','title_kz', 'subtitle_kz', 'content_kz', 'condition_kz'], 'required'],
            [['content', 'condition','content_kz', 'condition_kz'], 'string'],
            [['title', 'subtitle', 'title_kz', 'subtitle_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'subtitle' => 'Подзаголовок',
            'content' => 'Содержание',
            'condition' => 'Условия',
            'title_kz' => 'Заголовок (KZ)',
            'subtitle_kz' => 'Подзаголовок (KZ)',
            'content_kz' => 'Содержание (KZ)',
            'condition_kz' => 'Условия (KZ)',
        ];
    }

    public function getTitle(){
        $title = "title".Yii::$app->session["lang"];
        return $this->$title;
    }

    public function getSubTitle(){
        $text = "subtitle".Yii::$app->session["lang"];
        return $this->$text;
    }

    public function getContent(){
        $content = "content".Yii::$app->session["lang"];
        return $this->$content;
    }

    public function getCondition(){
        $condition = "condition".Yii::$app->session["lang"];
        return $this->$condition;
    }

    public static function getAll(){
        return ProductReturn::find()->one();
    }


}
