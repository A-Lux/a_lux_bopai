<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user_profile".
 *
 * @property int $user_id
 * @property string $name
 * @property string $surname
 * @property string $father
 * @property string $telephone
 * @property string $address
 * @property string $created_at
 * @property string $updated_at
 */
class UserProfile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'father', 'telephone', 'address','locality_id'], 'required'],
            [['user_id','locality_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'surname', 'father', 'telephone', 'address'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'father' => Yii::t('app', 'Father'),
            'telephone' => Yii::t('app', 'Phone number'),
            'locality_id' => Yii::t('app', 'Locality'),
            'address' => Yii::t('app', 'Address'),
            'created_at' => 'Дата создание',
            'updated_at' => 'Последные редактирование',
        ];
    }

    public static function getProfile(){
        return UserProfile::findOne(['user_id'=>Yii::$app->user->identity->id]);
    }

    public function getCity(){
        return $this->hasOne(DeliveryPrice::className(),['id' => 'locality_id']);
    }
}
