<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $fio
 * @property string $telephone
 * @property string $date
 * @property int $isRead
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'telephone', 'date', 'isRead'], 'required'],
            [['isRead'], 'integer'],
            [['fio', 'telephone'], 'string', 'max' => 255],
            [['date','created'],'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'Имя',
            'telephone' => 'Телефон',
            'date' => 'Дата',
            'isRead' => 'Прочитано',
            'created' => 'Дата заявки',
        ];
    }

    public function saveRequest($name, $phone, $date){
        $this->fio = $name;
        $this->telephone = $phone;
        $this->date = $date;
        $this->isRead = 0;
        if($this->save(false)){
            $array = ['status' => 1];
        }else{
            $array = ['status' => 0];
        }
        return $array;
    }
}
