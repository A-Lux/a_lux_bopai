<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "catalog".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property int $level
 * @property int $sort
 * @property int $status
 * @property string $created_at
 * @property string $name_en
 * @property string $name_kz
 */
class Catalog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/partner/';

    public static function tableName()
    {
        return 'catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status','name_kz'], 'required'],
            [['parent_id', 'level', 'sort', 'status', 'attribute'], 'integer'],
            [['created_at'], 'safe'],
            [['name', 'name_kz'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родитель',
            'name' => 'Название',
            'name_kz' => 'Название (KZ)',
            'image' => 'Картинка',
            'content' => 'Содержание',
            'level' => 'Level',
            'sort' => 'Sort',
            'status' => 'Статус',
            'attribute' => 'Атрибуты',
            'created_at' => 'Дата создание'
        ];
    }


    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $model = Catalog::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }


    public function getName(){
        $name = "name".Yii::$app->session["lang"];
        return $this->$name;
    }

    public function getParent()
    {
        return $this->hasOne(Catalog::className(), ['id' => 'parent_id']);
    }

    public static function getList()
    {
        return \yii\helpers\ArrayHelper::map(Catalog::find()->all(), 'id', 'name');
    }

    public static function getCatalogList()
    {
        return \yii\helpers\ArrayHelper::map(Catalog::findAll(['level' => 1]), 'id', 'name');
    }


    public static function getCatalog(){
        return Catalog::find()
            ->where("level = 1")
            ->with('category')
            ->orderBy('sort', 'ASC')
            ->all();
    }

    public static function getListForProduct()
    {
        return \yii\helpers\ArrayHelper::map(Catalog::find()->where("parent_id > 0")->all(), 'id', 'name');
    }

    public function getCategory()
    {
        return $this->hasMany(Catalog::className(), ['parent_id' => 'id']);
    }



    public static function getProducts($id, $sql = "", $sort = "", $colors = "", $sizes = "")
    {
        if($id != null) {
            $arr = [];
            $partner = Catalog::find()->all();
            $arr[] = $id;
            foreach ($partner as $v) {
                if ($v->parent_id == $id) {
                    $arr[] = $v->id;
                    $level2 = Catalog::findAll(['parent_id' => $v->id]);
                    if ($level2 != null) {
                        foreach ($level2 as $v2) {
                            if ($v2->parent_id == $v->id) {
                                $arr[] = $v2->id;
                                $level3 = Catalog::findAll(['parent_id' => $v2->id]);
                                if ($level3 != null) {
                                    foreach ($level3 as $v3) {
                                        if ($v3->parent_id == $v2->id) {
                                            $arr[] = $v3->id;
                                            $level4 = Catalog::findAll(['parent_id' => $v3->id]);
                                            if ($level4 != null) {
                                                foreach ($level4 as $v4) {
                                                    if ($v4->parent_id == $v3->id) {
                                                        $arr[] = $v4->id;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $query = Product::find()->joinWith(['product_quantity']);
            $query = $query->where('catalog_id in (' . implode(',', $arr) . ')' . $sql.$colors.$sizes);
            if ($sort != null) $query = $query->orderBy($sort);
            else $query = $query->orderBy('id DESC');
            $product = $query->all();

        }else{
            $query = Product::find()->joinWith(['product_quantity']);
            $query = $query->where($sql.$colors.$sizes);
            if ($sort != null) $query = $query->orderBy($sort);
            else $query = $query->orderBy('id DESC');
            $product = $query->all();
        }


        return $product;

    }


    public static function getBreadcrums($id)
    {
        $lvl4 = Catalog::findOne(['id' => $id]);
        $arr[] = array("id" => $lvl4->id, "name" => $lvl4->getName());

        $lvl3 = Catalog::findOne(['id' => $lvl4->parent_id]);
        if($lvl3 != null){
            $arr[] = array("id" => $lvl3->id, "name" => $lvl3->getName());
            $lvl2 = Catalog::findOne(['id' => $lvl3->parent_id]);
            if($lvl2 != null){
                $arr[] = array("id" => $lvl2->id, "name" => $lvl2->getName());
                $lvl = Catalog::findOne(['id' => $lvl2->parent_id]);
                if($lvl != null){
                    $arr[] = array("id" => $lvl->id, "name" => $lvl->getName());
                }
            }
        }

        return $arr;
    }



    public function getLastParent()
    {
        $lvl4 = Catalog::findOne(['id' => $this->id]);
        $id = $lvl4->id;

        $lvl3 = Catalog::findOne(['id' => $lvl4->parent_id]);
        if($lvl3 != null){
            $id = $lvl3->id;
            $lvl2 = Catalog::findOne(['id' => $lvl3->parent_id]);
            if($lvl2 != null){
                $id = $lvl2->id;
                $lvl = Catalog::findOne(['id' => $lvl2->parent_id]);
                if($lvl != null){
                    $id = $lvl->id;
                }
            }
        }

        return Catalog::findOne($id);
    }


    public function getLastParentAttribute(){
        return $this->lastParent->attribute;
    }




}

