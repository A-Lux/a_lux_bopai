<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "address".
 *
 * @property int $id
 * @property string $city
 * @property string $content
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city', 'content','content_kz'], 'required'],
            [['content', 'content_kz'], 'string'],
            [['city'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'city' => 'Город',
            'content' => 'Описание',
            'content_kz' => 'Описание (KZ)',
        ];
    }


    public static function getAll(){
        return Address::find()->all();
    }


    public function getContent(){
        $content = "content".Yii::$app->session["lang"];
        return $this->$content;
    }
}
