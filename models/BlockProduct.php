<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "block_product".
 *
 * @property int $id
 * @property int $product_id
 * @property int $block_id
 */
class BlockProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'block_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'block_id'], 'required'],
            [['product_id', 'block_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Продукт',
            'block_id' => 'Блок',
        ];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    public function getBlock()
    {
        return $this->hasOne(BlockForProduct::className(), ['id' => 'block_id']);
    }


}
