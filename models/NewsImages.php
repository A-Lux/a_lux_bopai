<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news_images".
 *
 * @property int $id
 * @property string $image
 * @property int $news_id
 */
class NewsImages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $path = 'uploads/images/news/';
    public static function tableName()
    {
        return 'news_images';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['news_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg','maxFiles' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Внутренняя картинка',
            'news_id' => 'Новость или статьи',
        ];
    }

    public static function deleteImages($id){
        $images = NewsImages::findAll(['news_id' => $id]);
        foreach($images as $v){
            unlink($v->path . $v->image);
            $v->delete();
        }
    }

    public function getImage()
    {
        return ($this->image) ? '/'.$this->path . $this->image : '/no-image.png';
    }

}
