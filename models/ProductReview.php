<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_review".
 *
 * @property int $product_id
 * @property int $comment
 */
class ProductReview extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_review';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'comment'], 'required'],
            [['product_id', 'comment'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'comment' => 'Comment',
        ];
    }
}
