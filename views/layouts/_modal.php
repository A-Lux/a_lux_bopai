<?php

use yii\captcha\Captcha;

?>
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content" >
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?=Yii::t('app', 'entrance');?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <label for="uname" class="form-control-label"><?=Yii::t('app', 'E-mail');?>:</label>
                    <input type="email" class="form-control" name="LoginForm[username]">
                    <label for="upwd" class="form-control-label"><?=Yii::t('app', 'Password');?>:</label>
                    <input type="password" class="form-control" name="LoginForm[password]">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="go-to-register"><?=Yii::t('app', 'Sign upp');?>?</button>
                <button type="button" class="btn btn-primary" id="login-button"><?=Yii::t('app', 'sign in');?></button>
            </div>
        </form>
    </div>
</div>


<div class="modal fade" id="registration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">

        <form class="modal-content" action="/account/register">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><?=Yii::t('app', 'sign up');?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div>
                    <label class="form-control-label"><?=Yii::t('app', 'Name');?>:</label>
                    <input type="text" class="form-control" name="UserProfile[name]">
                    <label class="form-control-label"><?=Yii::t('app', 'Surname');?>:</label>
                    <input type="text" class="form-control" name="UserProfile[surname]">
                    <label class="form-control-label"><?=Yii::t('app', 'Father');?>:</label>
                    <input type="text" class="form-control" name="UserProfile[father]">

                    <label class="form-control-label"><?=Yii::t('app', 'Phone number');?>:</label>
                    <input type="text" class="form-control" name="UserProfile[telephone]" data-error="<?=Yii::t('app', 'You must fill in the "Phone".');?>">

                    <label class="form-control-label"><?=Yii::t('app', 'Locality');?>:</label>
                    <select class="form-control" name="UserProfile[locality_id]" >
                        <? foreach (Yii::$app->view->params['delivery-address'] as $v):?>
                            <option value="<?=$v->id;?>"><?=$v->city;?></option>
                        <? endforeach;?>
                    </select>

                    <label class="form-control-label"><?=Yii::t('app', 'Address');?>:</label>
                    <input type="text" class="form-control" name="UserProfile[address]" placeholder="<?=Yii::t('app', 'Street/ house/ flat');?>">

                    <label class="form-control-label"><?=Yii::t('app', 'E-mail');?>:</label>
                    <input type="text" class="form-control" name="SignupForm[email]" >

                    <label class="form-control-label"><?=Yii::t('app', 'Password');?>:</label>
                    <input type="password" class="form-control" name="SignupForm[password]">

                    <label class="form-control-label"><?=Yii::t('app', 'Password confirmation');?>:</label>
                    <input type="password" class="form-control" name="SignupForm[password_verify]">
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="haveAccount"><?=Yii::t('app', 'I have an account');?></button>
                <button type="button" class="btn btn-primary" id="register"><?=Yii::t('app', 'Sign upp');?></button>
            </div>
        </form>
    </div>
</div>

