<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\PublicAsset;
use yii\helpers\Html;
use yii\helpers\Url;

PublicAsset::register($this);



?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/public/img/favicon.ico" type="image/x-icon">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/awesomplete/1.1.5/awesomplete.min.js"></script>


    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>


<!-- HEADER -->
<?=$this->render('_header')?>
<!-- END HEADER -->

<!-- HEADER -->
<?=$this->render('_modal')?>
<!-- END HEADER -->

<?=$content?>

<!-- FOOTER -->
<?=$this->render('_footer')?>
<!-- END FOOTER -->

<?php $this->endBody() ?>
</body>

<script type="text/javascript">
    $(".form_datetime").datetimepicker({
        format: "dd MM yyyy hh:ii",
        autoclose: true,
        todayBtn: true,
        pickerPosition: "bottom-left"
    });
</script>
<script>
    $('.nextBtn').click(function(){
		$('.nav-tabs > .nav-item').next('li').find('a').trigger('click');
	});
	$('.previousBtn').click(function(){
		$('.nav-tabs > .nav-item').prev('li').find('a').trigger('click');
	});
</script>
</html>
<link rel="stylesheet" href="/public/css/awesomplete.css" />
<script src="//code.jivosite.com/widget.js" data-jv-id="63l8WBH40z" async></script>

<?php $this->endPage() ?>
