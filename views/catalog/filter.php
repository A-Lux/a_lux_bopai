<? if($products != null):?>
    <? foreach ($products as $v):?>
        <? if($v->quantity > 0):?>
        <? $v->images;$m++;?>
        <div class="tov_item">
            <a href="/product?id=<?=$v->id;?>" class="tov_item_img">
                <img src="<?=$v->getImage();?>" alt="">
            </a>
            <a href="/product?id=<?=$v->id;?>" class="tov-button"><?=Yii::t('app', 'More details');?></a>
            <div class="tov_item_info">
                <? if($v->isNew):?>
                    <div class="tov_new"><?=Yii::t('app', 'NEW!');?></div>
                <? endif;?>
                <? if($v->isPopular):?>
                    <div class="tov_new tov_pov"><?=Yii::t('app', 'Popular!');?></div>
                <? endif;?>
                <? if($v->isExclusive):?>
                    <div class="tov_new tov_exs"><?=Yii::t('app', 'Exclusive!');?></div>
                <? endif;?>
                <p><?=$v->getName();?></p>
                <? if($v->newPrice != null):?>
                    <div class="old_price"><?=$v->price;?><span>тг</span></div>
                    <div class="price"><?=$v->newPrice;?><span>тг</span></div>
                <? endif;?>
                <? if($v->newPrice == null):?>
                    <div class="price"><?=$v->price;?><span>тг</span></div>
                <? endif;?>
            </div>
        </div>
        <? endif;?>
    <? endforeach;?>
<? endif;?>
