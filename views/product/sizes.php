
<? if($sizes):?>
    <? foreach ($sizes as $k=>$v):?>
        <option data-id="<?=$v->size_id;?>" <?=($product->getQuantityBySize($v->size_id)) == 0 ? 'disabled = "disabled"':'';?>
            <?=$v->size_id == $selected_size_id ? "selected":"";?>><?=$product->getSize($v->size_id);?></option>
    <? endforeach;?>
<? endif;?>
