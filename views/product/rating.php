<div class="col-sm-12 col-md-5 star-rating pr-0">
    <div class="star-rating__wrap" >
        <? if(Yii::$app->user->isGuest):?><a href="#" data-toggle="modal" data-target="#login"><? endif;?>
            <input class="star-rating__input" id="star-rating-5" type="radio" name="rating" value="5" <?=Yii::$app->user->isGuest ? 'disabled':'';?> <?=($product->rating > 4 && $product->rating <= 5) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(5, <?=$product->id;?>)" <? endif;?>>
            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-5" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
            <input class="star-rating__input" id="star-rating-4" type="radio" name="rating" value="4" <?=Yii::$app->user->isGuest ? 'disabled':'';?> <?=($product->rating > 3 && $product->rating <= 4) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(4, <?=$product->id;?>)" <? endif;?>>
            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-4" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
            <input class="star-rating__input" id="star-rating-3" type="radio" name="rating" value="3" <?=Yii::$app->user->isGuest ? 'disabled':'';?> <?=($product->rating > 2 && $product->rating <= 3) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(3, <?=$product->id;?>)" <? endif;?>>
            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-3" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
            <input class="star-rating__input" id="star-rating-2" type="radio" name="rating" value="2" <?=Yii::$app->user->isGuest ? 'disabled':'';?> <?=($product->rating > 1 && $product->rating <= 2) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(2, <?=$product->id;?>)" <? endif;?>>
            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-2" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
            <input class="star-rating__input" id="star-rating-1" type="radio" name="rating" value="1" <?=Yii::$app->user->isGuest ? 'disabled':'';?>  <?=($product->rating > 0 && $product->rating <= 1) ? 'checked':''; ?> <? if(!Yii::$app->user->isGuest):?> onclick="addUserRating(1, <?=$product->id;?>)" <? endif;?>>
            <label class="star-rating__ico star-rating__hover fa fa-star-o fa-lg" for="star-rating-1" title="<?=Yii::t('app', 'Product Rating {0} of 5',$product->rating);?>"></label>
            <? if(Yii::$app->user->isGuest):?></a><? endif;?>
    </div>
</div>
<div class="col-sm-12 col-md-3 pl-0 pr-0">
    <p class="reviews"><?=$product->ratingCount;?> <?=Yii::t('app', 'reviews');?></p>
</div>
<div class="col-sm-12 col-md-4 pl-0 pr-0">
    <p class="bought"><?=Yii::t('app', 'Have bought');?> <?=$product->sales_quantity;?>
        <?=Yii::t('app', 'time');?> </p>
</div>