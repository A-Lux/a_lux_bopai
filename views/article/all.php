<div class="news pt-4 pb-5">
    <div class="container ">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <p class="main-page-link"><a href="/"><?=Yii::$app->view->params['main']->getText()?></a> <img src="/public/img/_.png"> <?=$model->getText();?></p>
            </div>
        </div>
    </div>
    <div class="news__containers container">
        <? $m=0;?>
        <? foreach ($articles as $v):?>
            <? $m++;?>
            <? if($m % 4 == 1):?>
            <div class="row">
            <? endif?>
                <div class="col-sm-12 col-md-3">
                    <div class="news__container">
                        <a href="#">
                            <img src="<?=$v->getImage()?>" alt="">
                        </a>
                        <a class="news-button" href="/article/view?id=<?=$v->id;?>"><?=Yii::t('app', 'Go to article');?></a>
                        <div>
                            <h2>
                               <?=$v->getName();?>
                            </h2>
                            <p>
                                <?=$v->getDate();?>
                            </p>
                        </div>
                    </div>
                </div>

            <? if($m % 4 == 0):?>
            </div>
            <? endif?>
        <? endforeach;?>
        <? if($m % 4 != 0):?>
        </div>
        <? endif?>

    </div>
</div>