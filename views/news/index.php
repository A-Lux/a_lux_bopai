
<div class="detailed">
    <? if(count($images)>0):?>
    <div class="background-slider">
        <div class="glide_header">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">

                    <? foreach ($images as $v):?>
                    <li class="glide__slide" style="background-image: url(<?=$v->getImage();?>)">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <p><?=$model->getDate();?></p>
                                    <h1><?=$model->getName();?></h1>
                                </div>
                            </div>
                        </div>
                    </li>
                    <? endforeach;?>
                </ul>
            </div>
        </div>
    </div>
    <? endif;?>
    <div class="detailed__container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <?=$model->getContent();?>
                </div>
            </div>
        </div>
    </div>
</div>