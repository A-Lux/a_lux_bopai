
<div class="result">
    <div class="container products-show">
        <div style="font-size: 22px;font-weight: bold;padding-top: 30px;padding-bottom: 5px;"><?=$keyword;?></div>
        <?php
        if($count){?>
            <div style="font-size: 15px;"> <?=Yii::t('app', 'Found');?> <?=$count;?>
                <?=Yii::t('app', 'of product');?></div>
        <?}else{?>
            <div style="font-size: 15px;padding-bottom: 300px;"><?=Yii::t('app', 'Sorry, no products were found upon request');?></div>
        <?}?>
        <div class="cont_tov">
            <? if($result != null):?>
            <? foreach ($result as $v):?>
                <div class="tov_item">
                    <a href="/product?id=<?=$v->id;?>" class="tov_item_img">
                        <img src="<?=$v->getImage();?>" alt="">
                    </a>
                    <a href="/product?id=<?=$v->id;?>" class="tov-button"><?=Yii::t('app', 'More details');?></a>
                    <div class="tov_item_info">
                        <? if($v->isNew):?>
                            <div class="tov_new"><?=Yii::t('app', 'NEW!');?></div>
                        <? endif;?>
                        <? if($v->isPopular):?>
                            <div class="tov_new tov_pov"><?=Yii::t('app', 'Popular!');?></div>
                        <? endif;?>
                        <? if($v->isExclusive):?>
                            <div class="tov_new tov_exs"><?=Yii::t('app', 'Exclusive!');?></div>
                        <? endif;?>
                        <p><?=$v->getName();?></p>
                        <? if($v->newPrice != null):?>
                            <div class="old_price"><?=$v->price;?><span>тг</span></div>
                            <div class="price"><?=$v->newPrice;?><span>тг</span></div>
                        <? endif;?>
                        <? if($v->newPrice == null):?>
                            <div class="price"><?=$v->price;?><span>тг</span></div>
                        <? endif;?>
                    </div>
                </div>
            <? endforeach;?>
            <? endif;?>
        </div>
    </div>
</div>
