<div class="feedback">
    <div class="container mt-4">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <p class="main-page-link">
                    <a href="/"><?=Yii::$app->view->params['main']->getText();?></a>
                    <img src="/public/img/_.png"> <?=$model->getText();?></p>
            </div>
        </div>
    </div>
    <div class="container mt-5 pb-5">
        <div class="feedback__container ">
            <h1><?=$model->getText();?></h1>
            <p>
                <?=Yii::t('app', 'You can order a call back, our operator will contact you and answer all your questions');?>
            </p>
            <form>
                <input type="text" placeholder="<?=Yii::t('app', 'Name');?>" name="Feedback[fio]" data-error-msg="<?=Yii::t('app', 'Fill in the "Name" field.');?>">
                <input type="text"  placeholder="<?=Yii::t('app', 'Phone number');?>" name="Feedback[telephone]" data-error-msg="<?=Yii::t('app', 'Fill in the field "Phone".');?>">
                <div class="input-append date form_datetime" >
                    <input size="16" type="text" placeholder="<?=Yii::t('app', 'date');?>" name="Feedback[date]" data-error-msg="<?=Yii::t('app', 'Fill in the Date field.');?>">
                    <span class="add-on">
                    <i class="icon-th"></i></span>
                </div>
                <input type="button" value="<?=Yii::t('app', 'Request a call');?>" id="sendFeedback"
                       data-success-msg-title="<?=Yii::t('app', 'Order shipped successfully!');?>"  data-success-msg-text="<?=Yii::t('app', 'We will contact you shortly.');?>">
            </form>
        </div>
    </div>
</div>