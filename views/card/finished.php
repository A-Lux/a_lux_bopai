<div class="finished">

    <div class="container">
        <div class="finished-container">
            <div class="container">
                <div class="title"><?=Yii::t('app', 'Order completed');?> <img src="/public/img/Vector.png" alt=""></div>
                <div class="subtitle"><?=Yii::t('app', 'Thank you for choosing the Bopai online store! You can track the status of your order in your account');?></div>
                <div class="time-order"><?=Yii::t('app', 'Expect a courier within 7-10 days');?></div>
                <? if(!Yii::$app->user->isGuest):?>
                    <a href="/account/orders"><input type="button" class="button-finished" value="<?=Yii::t('app', 'Go to your personal account');?>"></a>
                <? endif;?>
            </div>
            <div class="container mt-4">
                <div class="row">
                    <div class="col-12 title-products"><?=Yii::t('app', 'Products in order');?></div>
                </div>
                <? foreach ($_SESSION['basket'] as $v):?>
                    <div class="row order">
                        <div class="img">
                            <img src="<?=$v->getImage();?>" width="100%" alt="">
                        </div>
                        <div class="info">
                            <div class="name"><?=$v->name;?></div>
                            <div class="quantity">
                                <div class="qty"><?=$v->count;?> <?=Yii::t('app', 'pс.');?></div>
                                <div class="sum"><?=$v->sumProduct;?> тг.</div>
                            </div>
                        </div>
                    </div>
                <? endforeach;?>
            </div>
            <div class="container">
                <div class="strip"></div>
            </div>
            <div class="container">
                <div class="amount">
                    <p><?=Yii::t('app', 'Cost');?>:</p><span><?=$sumProduct;?> тг.</span>
                </div>
                <div class="amount">
                    <p><?=Yii::t('app', 'Cost of delivery');?>:</p><span><?=$deliveryPrice;?> тг.</span>
                </div>
                <div class="amount">
                    <p><?=Yii::t('app', 'Money transfer -4%');?>:</p><span><?=$percent;?> тг.</span>
                </div>
                <div class="amount">
                    <p><?=Yii::t('app', 'Total');?>:</p><span><?=$sum;?> тг.</span>
                </div>
            </div>
        </div>
    </div>

</div>

<?  unset($_SESSION['basket'],$_SESSION['delivery_id']);?>