<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.10.2019
 * Time: 11:06
 */

namespace app\controllers;


use app\models\Catalog;
use app\models\Product;
use app\models\ProductImage;
use app\models\ProductQuantity;
use app\models\ProductRating;
use app\models\Size;
use app\models\UserFavorite;
use yii\web\NotFoundHttpException;


class ProductController extends FrontendController
{


    public function actionIndex($id){

        $product = Product::findOne($id);
        if(!$product) throw new NotFoundHttpException();
        $this->setMeta($product->metaName, $product->metaKey, $product->metaDesc);
        $breadcrumbs = Catalog::getBreadcrums($product->catalog->id);
        if($product->catalog->lastParentAttribute == 3 || $product->catalog->lastParentAttribute == 2) $colors = $product->getProductColors();
        if($product->catalog->lastParentAttribute == 3) $sizes = $product->getProductSizes();
        if(!\Yii::$app->user->isGuest) $isFavorite = $product->getFavoriteStatus();

        return $this->render('index',compact('product','breadcrumbs','colors','sizes','isFavorite'));
    }


    public function actionGetColorSizes(){

        $color_id = $_GET['color_id'];
        $product_id = $_GET['product_id'];
        $selected_size_id = $_GET['selected_size_id'];
        $product = Product::findOne($product_id);
        $sizes = $product->getColorSizes($color_id);

        return $this->renderAjax('sizes',compact('product','sizes','selected_size_id'));
    }



    public function actionGetColorImages(){

        $images = ProductImage::findAll(['product_id' => $_GET['product_id'], 'color_id' => $_GET['color_id']]);
        return $this->renderAjax('images',compact('images'));
    }


    public function actionGetColorQuantity(){

        $product = Product::findOne($_GET['product_id']);
        if($_GET['selected_size_id'] != null)
            $quantity = $product->getQuantityByColorAndSize($_GET['color_id'],$_GET['selected_size_id']);
        else $quantity = $product->getQuantityByColor($_GET['color_id']);
        return $quantity;
    }



    public function actionGetSizeColors(){

        $size_id = $_GET['size_id'];
        $product_id = $_GET['product_id'];
        $selected_color_id = $_GET['selected_color_id'];
        $product = Product::findOne($product_id);
        $colors = $product->getSizeColors($size_id);

        return $this->renderAjax('colors',compact('product','colors','selected_color_id'));
    }


    public function actionGetSizeQuantity(){

        $product = Product::findOne($_GET['product_id']);
        if($_GET['selected_color_id'] != null)
            $quantity = $product->getQuantityByColorAndSize($_GET['selected_color_id'],$_GET['size_id']);
        else
            $quantity = $product->getQuantityBySize($_GET['size_id']);
        return $quantity;

    }



    public function actionAddBasket()
    {
        $product_id = $_GET['product_id'];
        $selected_color_id = $_GET['selected_color_id'];
        $selected_size_id = $_GET['selected_size_id'];

        $product = Product::findOne($product_id);


        $check = true;
        foreach ($_SESSION['basket'] as $k=>$v){
            if($v->id == $product_id && $v->color == $selected_color_id && $v->size == $selected_size_id){
                $check = false;
                break;
            }
        }
        if($check){
            $product->count = 1;
            $product->color = $selected_color_id;
            $product->size = $selected_size_id;
            array_push($_SESSION['basket'],$product);
        }

        if($product){
            $array = ['status' => 1,'count' => count($_SESSION['basket'])];
        }else{
            $array = ['status' => 0];
        }

        return json_encode($array);

    }


    public function actionAddProductToFavorite(){
        $favorite = UserFavorite::findByUserAndProduct(\Yii::$app->user->id, $_GET['product_id']);
        if($favorite == null){
            $newFavorite = new UserFavorite();
            $newFavorite->saveFavorite(\Yii::$app->user->id, $_GET['product_id']);
            $response = 1;
        }else{
            $favorite->delete();
            $response = 0;
        }
        return $response;
    }

    public function actionAddNewRating(){
        $rating = ProductRating::findOne(['product_id' => $_GET['product_id'],'user_id' => \Yii::$app->user->id]);
        $product = Product::findOne($_GET['product_id']);
        if($rating == null){
            $newFavorite = new ProductRating();
            $newFavorite->saveRating(\Yii::$app->user->id, $_GET['product_id'], $_GET['rating']);
        }else{
            $rating->updateRating($_GET['rating']);
        }

        return $this->renderAjax('rating',compact('product'));
    }


}