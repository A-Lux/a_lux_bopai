<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 29.04.2019
 * Time: 11:32
 */

namespace app\controllers;


use app\models\DeliveryPrice;
use app\models\Menu;
use app\models\OrderedProduct;
use app\models\Orders;
use app\models\Product;
use app\models\ProductQuantity;
use app\models\UserFavorite;
use app\models\UserProfile;
use Yii;
use yii\web\NotFoundHttpException;

class CardController extends FrontendController
{


    public function actionIndex(){

        $model = Menu::getModel("/card");
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $sum = Product::getSum();
        $countAllProduct = Product::getCount();

        return $this->render('basket',compact('sum','countAllProduct','model'));
    }



    public function actionCheckout(){

        if($_SESSION['basket'] == null){
            $this->redirect('index');
        }
        $model = Menu::getModel("/card");
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $countAllProduct = Product::getCount();
        $sumProduct = Product::getSum();

        $percent = intval(($sumProduct+Yii::$app->view->params['delivery-address'][0]->price)*4/100);


        if(!Yii::$app->user->isGuest){
            $user = UserProfile::findOne(Yii::$app->user->id);
            $deliveryPrice = DeliveryPrice::findOne($user->locality_id)->price;
        }else $deliveryPrice = Yii::$app->view->params['delivery-address'][0]->price;

        $sum = $percent+$deliveryPrice+$sumProduct;

        return $this->render('checkout',compact('sum','countAllProduct','user','percent','sumProduct','deliveryPrice'));
    }



    public function actionFinished($access_token)
    {
        $order = Orders::findOne(['access_token' => $access_token]);
        if(!$order){
            $this->redirect('/');
        }else{
            $model = Menu::getModel("/card");
            $order->updateAccessToken(null);
            $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
            $sumProduct = Product::getSum();
            $deliveryPrice = DeliveryPrice::findOne(['id' => $_SESSION['delivery_id']])->price;
            $percent = intval(($sumProduct+$deliveryPrice)*4/100);
            $sum = $percent+$deliveryPrice+$sumProduct;
            return $this->render('finished',compact('sumProduct','deliveryPrice','percent','sum'));
        }
    }


    public function actionOrderByDelivery(){

        $order = new Orders();

        $sumProduct = Product::getSum();
        $delivery = DeliveryPrice::findOne(['id' => $_GET['delivery_id']]);
        $percent = intval(($sumProduct+$delivery->price)*4/100);
        $sum = $percent+$delivery->price+$sumProduct;

        $_SESSION['delivery_id'] = $_GET['delivery_id'];

        if ($order->saveOrder($_GET['fio'],$_GET['email'],$_GET['phone'],$_GET['delivery_id'],$_GET['addressB'],$_GET['comment'], $sum,0,0,1)) {
            if($order->email != null){
                $this->sendInformationAboutPayment($order->fio, $order->email, $order->id,$sumProduct,$delivery->price,$percent,$sum);
            }
            foreach ($_SESSION['basket'] as $v) {
                $orderedProduct = new OrderedProduct();
                if($orderedProduct->saveProduct($order->id, $v->id, $v->count, $v->color, $v->size)){
                    $product = Product::findOne($orderedProduct->product_id);
                    $product->updateSalesQuantity($orderedProduct->count);

                    if($product->catalog->lastParentAttribute == 3) {
                        $product_quantity = ProductQuantity::findOne(['product_id' => $product->id, 'color_id' => $v->color, 'size_id' => $v->size]);
                        $product_quantity->updateQuantity($product_quantity->quantity - $orderedProduct->count);
                    }elseif($product->catalog->lastParentAttribute == 2) {
                        $product_quantity = ProductQuantity::findOne(['product_id' => $product->id, 'color_id' => $v->color]);
                        $product_quantity->updateQuantity($product_quantity->quantity - $orderedProduct->count);
                    }elseif($product->catalog->lastParentAttribute == 1) {
                        $product_quantity = ProductQuantity::findOne(['product_id' => $product->id]);
                        $product_quantity->updateQuantity($product_quantity->quantity - $orderedProduct->count);
                    }
                }
            }
            return $order->access_token;
        } else {
            return 0;
        }
    }


    public function actionDeleteProductFromBasket($product_id, $color_id, $size_id)
    {
        $m=0;
        foreach ($_SESSION['basket'] as $v){
            $product = Product::findOne($v->id);
            if($product->catalog->lastParentAttribute == 3){
                if($v->id == $product_id && $v->color == $color_id && $v->size == $size_id){
                    unset($_SESSION['basket'][$m]);
                    $_SESSION['basket'] = array_values($_SESSION['basket']);
                    break;
                }
            }elseif ($product->catalog->lastParentAttribute == 2){
                if($v->id == $product_id && $v->color == $color_id){
                    unset($_SESSION['basket'][$m]);
                    $_SESSION['basket'] = array_values($_SESSION['basket']);
                    break;
                }
            }elseif($product->catalog->lastParentAttribute == 1){
                if($v->id == $product_id){
                    unset($_SESSION['basket'][$m]);
                    $_SESSION['basket'] = array_values($_SESSION['basket']);
                    break;
                }
            }

            $m++;
        }
        return $this->redirect(['index']);
    }






    public function actionDownClicked()
    {

        $count = 1;
        $product_id = $_GET['product_id'];
        $color_id = $_GET['color_id'];
        $size_id = $_GET['size_id'];
        foreach ($_SESSION['basket'] as $k=>$v){
            $product = Product::findOne($v->id);
            if($product->catalog->lastParentAttribute == 3) {
                if($v->id == $product_id && $v->color == $color_id && $v->size == $size_id && $_SESSION['basket'][$k]->count > 1){
                    $_SESSION['basket'][$k]['count'] -= 1;
                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }elseif ($product->catalog->lastParentAttribute == 2) {
                if($v->id == $product_id && $v->color == $color_id  && $_SESSION['basket'][$k]->count > 1){
                    $_SESSION['basket'][$k]['count'] -= 1;
                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }elseif($product->catalog->lastParentAttribute == 1 && $_SESSION['basket'][$k]->count > 1){
                if($v->id == $product_id){
                    $_SESSION['basket'][$k]['count'] -= 1;
                    $count = $_SESSION['basket'][$k]['count'];
                    break;
                }
            }
        }

        $sumProduct = Product::getSum();
        $countAllProduct = Product::getCount();
        $deliveryPrice = DeliveryPrice::findOne(['id' => $_GET['delivery_id']])->price;
        $percent = intval(($sumProduct+$deliveryPrice)*4/100);
        $sum = $percent+$deliveryPrice+$sumProduct;
        $array = ['countProduct' => $count,'countAllProduct' => $countAllProduct,'sum' => $sum,
            'sumProduct' => $sumProduct, 'deliveryPrice' => $deliveryPrice, 'percent' => $percent];
        return json_encode($array);
    }


    public function actionUpClicked()
    {

        $count = 1;
        $status = 1;
        $product_id = $_GET['product_id'];
        $color_id = $_GET['color_id'];
        $size_id = $_GET['size_id'];
        foreach ($_SESSION['basket'] as $k=>$v){
            $product = Product::findOne($v->id);
            if($product->catalog->lastParentAttribute == 3) {
                if ($v->id == $product_id && $v->color == $color_id && $v->size == $size_id) {
                    $product_quantity = ProductQuantity::findOne(['product_id' => $product->id, 'color_id' => $v->color, 'size_id' => $v->size]);
                    if($product_quantity->quantity <= $_SESSION['basket'][$k]['count']){
                        $status = 0;
                    }else{
                        $_SESSION['basket'][$k]['count'] += 1;
                        $count = $_SESSION['basket'][$k]['count'];
                    }
                    break;

                }
            }elseif ($product->catalog->lastParentAttribute == 2) {
                if ($v->id == $product_id && $v->color == $color_id) {
                    $product_quantity = ProductQuantity::findOne(['product_id' => $product->id, 'color_id' => $v->color]);
                    if($product_quantity->quantity <= $_SESSION['basket'][$k]['count']){
                        $status = 0;
                    }else{
                        $_SESSION['basket'][$k]['count'] += 1;
                        $count = $_SESSION['basket'][$k]['count'];
                    }
                    break;
                }
            }elseif($product->catalog->lastParentAttribute == 1) {
                if ($v->id == $product_id) {
                    $product_quantity = ProductQuantity::findOne(['product_id' => $product->id]);
                    if($product_quantity->quantity <= $_SESSION['basket'][$k]['count']){
                        $status = 0;
                    }else{
                        $_SESSION['basket'][$k]['count'] += 1;
                        $count = $_SESSION['basket'][$k]['count'];
                    }
                    break;
                }
            }

        }

        $sumProduct = Product::getSum();
        $countAllProduct = Product::getCount();
        $deliveryPrice = DeliveryPrice::findOne(['id' => $_GET['delivery_id']])->price;
        $percent = intval(($sumProduct+$deliveryPrice)*4/100);
        $sum = $percent+$deliveryPrice+$sumProduct;
        $array = ['countProduct' => $count,'countAllProduct' => $countAllProduct,'sum' => $sum,
            'sumProduct' => $sumProduct, 'deliveryPrice' => $deliveryPrice, 'percent' => $percent,'status' => $status,'error' => Yii::t('app', 'There are no more such products.')];
        return json_encode($array);
    }

    public function actionUpdateDeliveryPrice(){

        $sumProduct = Product::getSum();
        $deliveryPrice = DeliveryPrice::findOne(['id' => $_GET['delivery_id']])->price;
        $percent = intval(($sumProduct+$deliveryPrice)*4/100);
        $sum = $percent+$deliveryPrice+$sumProduct;
        $array = ['sum' => $sum, 'deliveryPrice' => $deliveryPrice, 'percent' => $percent];
        return json_encode($array);
    }



    private function sendInformationAboutPayment($fio, $email, $id, $sumProduct,$deliveryPrice,$percent,$sum) {

        $host = Yii::$app->request->hostInfo;
        $orderedProduct = "<table>
			<colgroup><col width=\"72\">
			<col>
			<col width=\"100\">
			<col width=\"75\">
			<col width=\"125\"></colgroup>
			<tbody>
			    <tr>
                    <th></th>
                    <th>Наименование</th>
                    <th>Цена за ед.</th>
                    <th>Цвет</th>
                    <th>Размер</th>
                    <th>Кол-во</th>
                    <th>Сумма</th>
                </tr>";

        foreach ($_SESSION['basket'] as $v) {
            if ($v->newPrice != null) $price = $v->newPrice;
            else $price = $v->price;

            $productPrice = $v->sumProduct;
            $image = $v->getImage();
            $color = $v->getColor($v->color);
            $size = $v->getSize($v->size);
            $orderedProduct.= "<tr>
                    <td>
                        <img src=\"$host$image\" alt=\"\" class=\"CToWUd\">
                    </td>
                        <td><b> $v->name</b><br>
                    </td>
                    <td align=\"center\">$price тг</td>
                    <td align=\"center\">$v->count</td>
                    <td align=\"center\">$productPrice тг</td>
                </tr>";
        }
        $orderedProduct.= "<tr>
                <td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
				<td colspan=\"2\">------------</td>
				<td align=\"center\"></td></tr> 
				<tr><td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
				<td colspan=\"2\">Итого товара на сумму:</td>
				<td align=\"center\">$sumProduct тг</td></tr>
				
				<tr><td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
				<td colspan=\"2\">Стоимость доставки:</td>
				<td align=\"center\">$deliveryPrice тг</td></tr>
				
				<tr><td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
				<td colspan=\"2\">Перевод денег -4%:</td>
				<td align=\"center\">$percent тг</td></tr>
				
				<tr><td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
                <td colspan=\"2\"></td>
				<td colspan=\"2\">Итого:</td>
				<td align=\"center\">$sum тг</td></tr>
			
			</tbody></table>";
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
            ->setTo($email)
            ->setSubject('Поступил новый заказ №'.$id.' от '.date('d-m-Y h:i', time()))

            ->setHtmlBody("<p>$fio, благодарим Вас за оформление заказа на нашем сайте BOPAI.KZ.</p></br>
                                 <p>Ниже, предоставляем Вам информацию о заказах:</p></br></br>
                                 $orderedProduct
                                 </br></br>
                                 <p>---</p></br>
                                 <p>С уважением</p></br>
                                 <p>администрация <a href='$host'>BOPAI.KZ</a></p>");

        return $emailSend->send();

    }




}