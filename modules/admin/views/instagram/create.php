<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instagram */

$this->title = 'Создание';
$this->params['breadcrumbs'][] = ['label' => 'Ссылки для инстаграма', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instagram-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
