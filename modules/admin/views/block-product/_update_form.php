<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BlockProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="block-product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'block_id')->dropDownList(\app\models\BlockForProduct::getList()) ?>

    <div class="form-group">
        <select class="form-control select2 block-product" multiple="multiple" data-placeholder="Выберите продукта для блока" style="width: 100%;" name="BlockProduct[product_id]">
            <? foreach ($product as $v):?>
                <option value="<?=$v->id;?>" ><?=$v->name;?></option>
            <? endforeach;?>
        </select>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
