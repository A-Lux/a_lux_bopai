<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = ' Продукты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'format' => 'raw',
                'attribute' => 'price',
                'value' => function($data){
                    return $data->price." KZT";
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'newPrice',
                'value' => function($data){
                    return $data->newPrice != null?$data->newPrice." KZT":"";
                }
            ],
            //'content:ntext',
            //'description:ntext',
//            'sales_quantity',
            [
                'attribute' => 'isNew',
                'filter' => \app\modules\admin\status\LabelNew::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelNew::statusLabel($model->isNew);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isPopular',
                'filter' => \app\modules\admin\status\LabelPopular::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelPopular::statusLabel($model->isPopular);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isExclusive',
                'filter' => \app\modules\admin\status\LabelExclusive::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelExclusive::statusLabel($model->isExclusive);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isBackInStock',
                'filter' => \app\modules\admin\status\LabelBackInStock::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelBackInStock::statusLabel($model->isBackInStock);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'isHit',
                'filter' => \app\modules\admin\status\LabelHit::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelHit::statusLabel($model->isHit);
                },
                'format' => 'raw',
            ],


            [
                'attribute'=>'catalog_id',
                'filter'=>\app\models\Catalog::getListForProduct(),
                'value' => function ($model) {
                    return
                        Html::a($model->catalog->name, ['/admin/catalog/view', 'id' => $model->catalog->id]);
                },
                'format' => 'raw',
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
