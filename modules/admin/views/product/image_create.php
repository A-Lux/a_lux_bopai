<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductImage */

$this->title = 'Добавления картинки';

$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $product->name, 'url' => ['view', 'id' => $product->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-image-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_image_form', [
        'model' => $model,
        'product' => $product
    ]) ?>

</div>
