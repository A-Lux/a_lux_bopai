<?php

use app\models\Catalog;
use app\models\ProductQuantity;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form" style="padding-bottom: 2500px;">

    <?php if(Yii::$app->session->getFlash('quantity_error')):?>
        <div class="alert alert-danger" role="alert">
            <?= Yii::$app->session->getFlash('quantity_error'); ?>
        </div>
    <?php endif;?>

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">

            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru" aria-selected="false" class="nav-link">RU</a>
            </li>
            <li class="nav-item">
                <a id="kz-tab" data-toggle="tab" href="#kz" role="tab" aria-controls="kz" aria-selected="false" class="nav-link">KZ</a>
            </li>
            <li class="nav-item">
                <a id="status-tab" data-toggle="tab" href="#status" role="tab" aria-controls="status" aria-selected="false" class="nav-link">Статусы</a>
            </li>
            <li class="nav-item">
                <a id="discount-tab" data-toggle="tab" href="#discount" role="tab" aria-controls="discount" aria-selected="false" class="nav-link">Скидка</a>
            </li>

            <li class="nav-item">
                <a id="recomend-tab" data-toggle="tab" href="#recomend" role="tab" aria-controls="recomend" aria-selected="false" class="nav-link">Рекомендация</a>
            </li>

            <li class="nav-item">
                <a id="alsoBuys-tab" data-toggle="tab" href="#alsoBuys" role="tab" aria-controls="alsoBuys" aria-selected="false" class="nav-link">С этим также покупают</a>
            </li>

            <li class="nav-item">
                <a id="quantity-tab" data-toggle="tab" href="#quantity" role="tab" aria-controls="quantity" aria-selected="false" class="nav-link">Количества товара</a>
            </li>

        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">

            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">


                <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc')->textInput(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey')->textInput(['rows' => 6]) ?>

            </div>

            <div id="quantity" role="tabpanel" aria-labelledby="quantity-tab" class="tab-pane fade">
                <div id = 'color'></div>
                <div id="size"></div>
                <div class="form-group field-productQuantity-price required has-success">
                    <label class="control-label" for="productQuantity-price">Количества</label>
                    <input type="number" id="productQuantity-price" class="form-control" name="ProductQuantity[quantity]"  aria-required="true" aria-invalid="false">
                    <div class="help-block"></div>
                </div>

                <div class="form-group" style="padding-bottom: 20px;">
                    <button type="button" class="btn btn-success" id="add-quantity">Добавить</button>
                </div>

                <div class="box" id = 'result'>
                    <div class="box-header">
                        <h3 class="box-title">Количества товара</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Цвет</th>
                                <th>Размер</th>
                                <th>Количества</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <? if($_SESSION["quantity_product"] != null):?>
                                <? foreach ($_SESSION["quantity_product"] as $v):?>
                                    <? $color = ProductQuantity::getColorById($v["color"]);$size = ProductQuantity::getSizeById($v["size"]);?>
                                    <tr>
                                        <td><?=$color != null?$color->name:"Нет";?></td>
                                        <td><?=$size;?></td>
                                        <td><?=$v["quantity"];?></td>
                                        <td>
                                            <button type="button" class="btn btn-sm removeQuantity" data-id="<?=$v["id"];?>">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </td>
                                    </tr>
                                <? endforeach;?>
                            <? endif;?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>


            </div>



            <div id="alsoBuys" role="tabpanel" aria-labelledby="alsoBuys-tab" class="tab-pane fade">

                <div class="form-group">
                    <select class="form-control select2 alsoBuys" multiple="multiple" data-placeholder="Выберите продукта" style="width: 100%;" name="ProductAlsoBuys[also_buys_product_id][]">
                        <? foreach ($products as $v):?>
                            <option value="<?=$v->id;?>" <?=in_array($v->name,$alsoBuys_products)?"selected":"";?>><?=$v->name;?></option>
                        <? endforeach;?>
                    </select>
                </div>

            </div>


            <div id="recomend" role="tabpanel" aria-labelledby="recomend-tab" class="tab-pane fade">

                <div class="form-group">
                    <select class="form-control select2 recomend" multiple="multiple" data-placeholder="Выберите продукта" style="width: 100%;" name="ProductRecomend[recoment_product_id][]">
                        <? foreach ($products as $v):?>
                            <option value="<?=$v->id;?>" <?=in_array($v->name,$recomend_products)?"selected":"";?>><?=$v->name;?></option>
                        <? endforeach;?>
                    </select>
                </div>

            </div>


            <div id="discount" role="tabpanel" aria-labelledby="discount-tab" class="tab-pane fade">

                <?= $form->field($model, 'newPrice')->textInput() ?>

            </div>

            <div id="status" role="tabpanel" aria-labelledby="status-tab" class="tab-pane fade">

                <?= $form->field($model, 'isNew')->dropDownList(\app\modules\admin\status\LabelNew::statusList()) ?>

                <?= $form->field($model, 'isPopular')->dropDownList(\app\modules\admin\status\LabelPopular::statusList()) ?>

                <?= $form->field($model, 'isExclusive')->dropDownList(\app\modules\admin\status\LabelExclusive::statusList()) ?>

                <?= $form->field($model, 'isBackInStock')->dropDownList(\app\modules\admin\status\LabelBackInStock::statusList()) ?>

                <?= $form->field($model, 'isHit')->dropDownList(\app\modules\admin\status\LabelHit::statusList()) ?>

            </div>

            <div id="ru" role="tabpanel" aria-labelledby="ru-tab" class="tab-pane fade">


                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'manufacturer')->textInput() ?>

                <?php
                echo $form->field($model, 'content')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

                <?php
                echo $form->field($model, 'description')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

                <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc')->textInput(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey')->textInput(['rows' => 6]) ?>

            </div>

            <div id="kz" role="tabpanel" aria-labelledby="kz-tab" class="tab-pane fade">

                <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'manufacturer_kz')->textInput() ?>

                <?php
                echo $form->field($model, 'content_kz')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

                <?php
                echo $form->field($model, 'description_kz')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);
                ?>

                <?= $form->field($model, 'metaName_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc_kz')->textInput(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey_kz')->textInput(['rows' => 6]) ?>

            </div>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">


                <?= $form->field($model, 'price')->textInput() ?>

                <?php
                echo $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>

                <?= $form->field($model, 'catalog_id')->dropDownList(\app\models\Catalog::getListForProduct()) ?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>



<script>


    $(document).ready(function() {
        $.ajax({
            url: "/admin/product/get-color",
            type: 'GET',
            data: {id: $('#product-catalog_id option:selected').val()},
            success: function (response) {
               $('#color').html(response);
            },
            error: function () {
                alert('FAILED');
            }
        });

        $.ajax({
            url: "/admin/product/get-size",
            type: 'GET',
            data: {id: $('#product-catalog_id option:selected').val()},
            success: function (response) {
                $('#size').html(response);
                $('.size').select2();
            },
            error: function () {
                alert('FAILED');
            }
        });
    });


    $('#product-catalog_id').change(function(){

        $.ajax({
            url: "/admin/product/get-color",
            type: 'GET',
            data: {id: $('#product-catalog_id option:selected').val()},
            success: function (response) {
                $('#color').html(response);
            },
            error: function () {
                alert('FAILED');
            }
        });

        $.ajax({
            url: "/admin/product/get-size",
            type: 'GET',
            data: {id: $('#product-catalog_id option:selected').val()},
            success: function (response) {
                $('#size').html(response);
                $('.size').select2();
            },
            error: function () {
                alert('FAILED');
            }
        });
    });

   $('body').on('click','#add-quantity', function (e) {
       e.preventDefault();
       var color = $('select[name="ProductQuantity[color_id]"]').val();
       var sizes = $('select[name="ProductQuantity[size_id][]"]').val();
       var quantity = $('input[name="ProductQuantity[quantity]"]').val();
       var size = '';

       if (typeof color === 'undefined'){
           color = "";
       }

       if(typeof sizes === 'undefined'){
           size = '';
       }else{
           if(sizes == ""){
               alert("Необходимо указать размер.");
           }else{
               for(var i=0;i<sizes.length;i++){
                   size += sizes[i]+',';
               }
               size = size.substring(0, size.length - 1);
           }

       }


       if(quantity == ""){
           alert("Необходимо указать количества товара.");
       }else{
           $.ajax({
               url: "/admin/product/add-quantity",
               type: "GET",
               data: {color:color,size:size,quantity:quantity},
               success: function(response){
                   $('#result').html(response);
               },
               error: function () {
                   alert('FAILED');
               }
           });
       }
   });

   $('body').on('click','.removeQuantity', function (e) {
       e.preventDefault();
       var id = $(this).attr("data-id");

       $.ajax({
           url: "/admin/product/remove-quantity",
           type: "GET",
           data: {id:id},
           success: function(response){
               $('#result').html(response);
           },
           error: function () {
               alert('FAILED');
           }
       });
   });


   $('body').on('click','#add-images', function(){

       $.ajax({
           type: 'POST',
           url: '/admin/product/add-images',
           data: $(this).closest('form').serialize(),
           success: function (response) {
               if (response == 1) {
                   $('input[name = "Subscription[email]"]').val('');
                   swal('Вы успешно', ' подписаны.', 'success');
               } else {
                   swal('Ошибка!', response, 'error');
               }
           },
           error: function () {
               swal('Упс!', 'Что-то пошло не так.', 'error');
           }
       });
   });


</script>

