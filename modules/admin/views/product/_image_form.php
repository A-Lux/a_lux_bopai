<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProductImage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-image-form">

    <?php $form = ActiveForm::begin(); ?>

    <? if($product->catalog->lastParentAttribute == 3 || $product->catalog->lastParentAttribute == 2):?>
        <?= $form->field($model, 'color_id')->dropDownList(\app\models\Color::getList()) ?>
    <? endif;?>

    <?=$form->field($model, 'image[]')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => [
            'multiple' => true,
            'accept' => 'image/*'
        ],
    ]);
    ?>

    <div class="form-group" style="padding-bottom: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
