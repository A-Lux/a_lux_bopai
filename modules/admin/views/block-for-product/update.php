<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BlockForProduct */

$this->title = 'Редактирование блока: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Блоки для продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="block-for-product-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
