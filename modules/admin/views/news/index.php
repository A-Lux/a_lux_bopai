<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи и новости';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',

            //'content:ntext',
            [
                'attribute' => 'type',
                'filter' => \app\modules\admin\status\LabelNewsAndArticle::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelNewsAndArticle::statusLabel($model->type);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\status\Label::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'date',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{send-all} {view} {update} {delete}',
                'buttons'=>[
                    'send-all' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-envelope"></span>', '/admin/news/send-all?id='.$model->id, [
                            'title' => Yii::t('yii', 'Отправить всем '),
                        ]);
                    },

                ]
            ],
        ],
    ]); ?>
</div>
