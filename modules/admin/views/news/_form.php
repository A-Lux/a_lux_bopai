<?php

use kartik\date\DatePicker;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form" style="padding-bottom: 2000px;">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <li class="nav-item">
                <a id="ru-tab" data-toggle="tab" href="#ru" role="tab" aria-controls="ru" aria-selected="false" class="nav-link">RU</a>
            </li>
            <li class="nav-item">
                <a id="kz-tab" data-toggle="tab" href="#kz" role="tab" aria-controls="kz" aria-selected="false" class="nav-link">KZ</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="ru" role="tabpanel" aria-labelledby="ru-tab" class="tab-pane fade">


                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>


                <?= $form->field($model, 'content')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);?>

                <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc')->textInput(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey')->textInput() ?>

            </div>

            <div id="kz" role="tabpanel" aria-labelledby="kz-tab" class="tab-pane fade">


                <?= $form->field($model, 'name_kz')->textInput(['maxlength' => true]) ?>


                <?= $form->field($model, 'content_kz')->widget(CKEditor::className(),[
                    'editorOptions' => [
                        'preset' => 'full', // basic, standard, full
                        'inline' => false, //по умолчанию false
                    ],
                ]);?>

                <?= $form->field($model, 'metaName_kz')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc_kz')->textInput(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey_kz')->textInput() ?>

            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]);
                ?>


                <?= $form->field($images, 'image[]')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload' => false ,
                    ] ,
                    'options' => [
                        'multiple' => true,
                        'accept' => 'image/*'
                    ],
                ]);
                ?>


                <?= $form->field($model, 'type')->dropDownList(\app\modules\admin\status\LabelNewsAndArticle::statusList()) ?>

                <?= $form->field($model, 'status')->dropDownList(\app\modules\admin\status\Label::statusList()) ?>


                <?= DatePicker::widget([
                    'name'=>'News[date]',
                    'value' => $model->date,
                    'options' => ['placeholder' => 'Выберите дату...'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]);
                ?>

            </div>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
