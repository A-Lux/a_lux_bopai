<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Catalog */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Каталог', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="catalog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'parent_id',
                'value' => function ($model) {
                    return
                        Html::a($model->parent->name, ['view', 'id' => $model->parent->id]);
                },
                'format' => 'raw',
            ],
            'name',
            'name_kz',

            [
                'attribute' => 'attribute',
                'filter' => \app\modules\admin\status\LabelAttribute::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\LabelAttribute::statusLabel($model->getLastParentAttribute());
                },
                'format' => 'raw',
            ],


            [
                'attribute' => 'status',
                'filter' => \app\modules\admin\status\Label::statusList(),
                'value' => function ($model) {
                    return \app\modules\admin\status\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>



</div>
