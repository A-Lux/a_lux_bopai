<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\SourceMessage */

$this->title = $model->message;
$this->params['breadcrumbs'][] = ['label' => 'Переводы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="source-message-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>


     <table id="w0" class="table table-striped table-bordered detail-view">
        <tbody>
            <? foreach ($message as $v):?>
                <tr>
                    <th style="text-transform: uppercase"><?=$v->language;?></th>
                    <td><?=$v->translation;?></td>
                </tr>
            <? endforeach;?>
        </tbody>
    </table>

</div>
