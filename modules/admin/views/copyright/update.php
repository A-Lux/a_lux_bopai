<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Copyright */

$this->title = 'Редактирование  ';
$this->params['breadcrumbs'][] = ['label' => 'Копирайт', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="copyright-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
