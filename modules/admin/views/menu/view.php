<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = $model->text;
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return \app\modules\admin\status\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],

            'text',
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
            'text_kz',
            'metaName_kz',
            'metaDesc_kz:ntext',
            'metaKey_kz:ntext',
        ],
    ]) ?>

</div>
