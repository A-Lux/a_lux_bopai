<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orders-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telephone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


    <?= $form->field($model, 'status_delivery')->dropDownList(\app\modules\admin\status\LabelDelivery::statusList()) ?>

    <?= $form->field($model, 'status_payment')->dropDownList(\app\modules\admin\status\LabelPayment::statusList()) ?>

    <?= $form->field($model, 'payment_method')->dropDownList(\app\modules\admin\status\LabelPaymentMethod::statusList(),['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'sum')->input('text', ['disabled' => 'disabled']) ?>

    <?= $form->field($model, 'order_date')->input('datetime', ['disabled' => 'disabled']) ?>

    <div class="form-group" style="padding-bottom: 20px;">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
