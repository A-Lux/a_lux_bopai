<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 25.02.2019
 * Time: 17:08
 */

namespace app\assets;

use yii\web\AssetBundle;


class publicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
       	"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
        "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css",
        "/public/css/bootstrap-datetimepicker.css",
        "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css",
        "/public/hc-offcanvas-nav-master/dist/hc-offcanvas-nav.css",
        "/public/libs/owlcarousel/assets/owl.carousel.min.css",
        "/public/libs/owlcarousel/assets/owl.theme.default.min.css",
        "https://cdn.bootcss.com/Glide.js/3.3.0/css/glide.core.css",
        "https://cdn.bootcss.com/Glide.js/3.3.0/css/glide.theme.css",
        "/public/css/style.css",
        "/public/slick/slick.css",
        "/public/slick/slick-theme.css",
    ];
    public $js = [
        "/public/slick/slick.min.js",
        "/public/hc-offcanvas-nav-master/dist/hc-offcanvas-nav.js",
        "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js",
        "/public/libs/owlcarousel/owl.carousel.min.js",
        "/public/js/bootstrap-datetimepicker.min.js",
        "https://cdn.jsdelivr.net/npm/@glidejs/glide",
        "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js",
        "/public/js/main.js",
        "/public/js/account.js",
        "/public/js/card.js",
        "/public/js/product.js",
        "/public/js/site.js",
        "/public/js/filter.js",
    ];
    public $depends = [

    ];
}